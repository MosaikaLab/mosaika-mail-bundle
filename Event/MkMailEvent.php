<?php
namespace Mosaika\MailBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class MkMailEvent extends Event
{
    
    const NAME = 'mk.mail.send';

    /**
     * 
     * @var string
     */
    protected $type;
        
    /**
     * 
     * @var array
     */
    protected $mailHeaders = array(
        'from'=>null,
        'to'=>null,
        'subject'=>null,
        'attach'=>null
    );
    
    
    /**
     *
     * @var array
     */
    protected $vars;
    
    /**
     * @var \Swift_Mailer
     */
    protected $mailer;

    public function __construct($type,$mailHeaders,$vars, \Swift_Mailer $mailer){
        $this->type = $type;
        $this->mailHeaders = $mailHeaders;
        $this->vars = $vars;
        
        $this->mailer = $mailer;
    }
    
    public function setType($t){
        $this->type = $t;
        return $this;
    }
    
    public function getType(){
        return $this->type;
    }
    
    public function setMailHeaders($headers){
        $this->mailHeaders = $headers;
        return $this; 
    }
    
    public function getMailHeaders(){
        return $this->mailHeaders;
    }
    
    public function setVars($v){
        $this->vars = $v;
        return $this;
    }
    public function getVars(){
        return $this->vars;
    }
    
    public function getMailer(){
        return $this->mailer;
    }
    

}

