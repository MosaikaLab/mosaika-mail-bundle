<?php
namespace Mosaika\MailBundle\Repository;
use App\Utils\DatatableRequest;

class MailRepository extends \Doctrine\ORM\EntityRepository{
    const ID = 'MailBundle:Mail';


    public function search(DatatableRequest $request){
		$qb = $this->_em->createQueryBuilder();
		$qb->select('m')
		->from($this->_entityName, 'm');
		if($request->getSearch()){
            $qb->andWhere("m.name like :s")->setParameter("s", "%" . $request->getSearch() . "%");
            $qb->orWhere("m.text like :s")->setParameter("s", "%" . $request->getSearch() . "%");
		}
		
		
		$totQuery = clone $qb;
		$tot = $totQuery->select("count (distinct m.id)")->getQuery()->getSingleResult();
		$this->total = $tot[1];
		
		$qb
		->setFirstResult($request->getStart())
		->setMaxResults($request->getLimit())
		->addOrderBy("m.name","ASC")
		;
		return $qb->getQuery()->getResult();
    }
    
    public function getTotal(){
		return $this->total;
	}

}