<?php
namespace Mosaika\MailBundle\Subscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Mosaika\MailBundle\Event\MkMailEvent;
use Symfony\Component\Yaml\Yaml;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\KernelInterface as BaseKernel;
use Mosaika\MailBundle\Entity\Mail;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Mosaika\MailBundle\Service\SkinService;
/**
 * 
 * @author Giuliano Gostinfini
 *
 */
class MailSender implements EventSubscriberInterface
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
	 * @var \Symfony\Bundle\FrameworkBundle\Templating\EngineInterface
	 */
    protected $templating;
    
    protected $skinService;
    
    public function __construct( BaseKernel $kernel, EntityManagerInterface $em, EngineInterface $templating)
    {
        $this->templating = $templating;
        $this->em = $em;
        $this->kernel = $kernel;
        $this->skinService = new SkinService($this->em,$this->kernel);
        try{
            $y = Yaml::parse(file_get_contents($this->kernel->getProjectDir().'/config/packages/mosaika_mail.yaml'));
            $this->mailModels = $y['mosaika_mail']['models'];
            $this->baseUrl = $_SERVER['SERVER_NAME'];
        }catch(\Exception $e){
            echo $e->getMessage();
        }
        
    }
    
    public static function getSubscribedEvents(){
        return array(
            "mk.mail.send" => array(array("send", 10))
        );
    }
    
    public function getModel($model){
        return $this->mailModels[$model];
    }
    
    /**
     * 
     * @param MkMailEvent $event
     */
    public function send($event){
        try{
            $name = $event->getType();
            $model = $this->getModel($name);
            $skin = $this->skinService->getModelSkin($name);
            $mailDb = $this->em->getRepository(Mail::class)->findOneByName($name);
            $defText = $mailDb->getText();
            $fromName = $mailDb->getSenderName();
            $subject = $mailDb->getSubject();
            $text = $this->compileText($defText, $event->getVars(), $model['variables']);
            $vs = $event->getMailHeaders();
            $senderEmail = strtolower(filter_var($vs["from"],FILTER_SANITIZE_EMAIL));
            

            $subject = !is_null($subject) ? $subject : $model['subject'];
            $subject = $this->compileText($subject, $event->getVars(), $model['variables']);
            if(is_null($fromName) && isset($model['from'])){
                $fromName = $model['from'];
            }elseif(is_null($fromName) && !isset($model['from'])){
                $fromName = substr($vs['from'], 0, strpos($vs['from'],'@'));
            }
            $fromName = $this->compileText($fromName, $event->getVars(), $model['variables']);
            $pre = ($skin['name'] == 'Default Skin' || $skin['name'] == 'Skin Blu') ? '@MosaikaMail/skins/' : '';
            $message = (new \Swift_Message($subject))
            ->setFrom($senderEmail,$fromName)
            ->setTo($vs['to'])
            ->setBody(
                $this->templating->render(
					$pre.$skin['file'],
					array(
						'content'=>$text
					)
			),
                'text/html'
                );
            if(isset($vs['attach']) && !is_null($vs['attach'])){
                $message->attach($vs['attach']);
            }
            if(isset($vs['replyto']) && !is_null($vs['replyto'])){
                $message->addReplyTo($vs['replyto']['mail'], $vs['replyto']['name']);
            }
            if(isset($vs['bcc']) && !is_null($vs['bcc'])){
                $message->setBcc($vs['bcc']);
            }
            if(isset($vs['cc']) && !is_null($vs['cc'])){
                $message->setCc($vs['cc']);
            }

            $mailer = $event->getMailer();
            $result = $mailer->send($message);
            return $result;
        }catch(\Exception $e){
            throw $e;
            echo $e->getFile().' '.$e->getLine().'<br>';
            echo $e->getMessage();
        }
    }
    
    protected function compileText($text, $receiver, $vars){
        $arr = array();
        foreach($vars as $k=>$v){
            $arr[$k] = $receiver[$k];
        }
        foreach($arr as $k => $v){
            $text = str_replace("[" . $k . "]", $v , $text);
        }
        return $text;
    }
    
}

