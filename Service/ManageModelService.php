<?php
namespace Mosaika\MailBundle\Service;


use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\HttpKernel\KernelInterface as BaseKernel;
use Mosaika\MailBundle\Entity\Mail;
use Mosaika\MailBundle\Helper\MailModelHelper;
use App\Utils\DatatableRequest;
use Symfony\Component\HttpFoundation\Request;

class ManageModelService{

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var Kernel
     */
    protected $kernel;

    public function __construct(EntityManagerInterface $em, BaseKernel $kernel)
    {
        
        $this->em = $em;
        $this->kernel = $kernel;
        $y = Yaml::parse(file_get_contents($this->kernel->getProjectDir().'/config/packages/mosaika_mail.yaml'));
        $this->models = $y['mosaika_mail']['models'];
    }

    public function modelsToDb(){
        try{
            foreach ($this->models as $name => $props) {
                if(!$this->modelExsists($name)){
                    $m = new Mail();
                    $m->setName($name);
                    $m->setText($props['defaultText']);
                    if(isset($props['defaultText']))$m->setSubject($props['subject']);
                    $this->em->persist($m);
                    $this->em->flush();
                }
            }
        }catch(\Exception $e){
            echo '<br>';
            echo PHP_EOL;
            echo $e->getMessage();
            echo '<br>';
            echo PHP_EOL;
        }

    }
    public function updateModelsSubject(){
        try{
            $repo = $this->em->getRepository(Mail::class);
            foreach ($this->models as $name => $props) {
                $m = $repo->findOneByName($name);
                if(!is_null($m) && is_null($m->getSubject())){
                    if(isset($props['subject']))$m->setSubject($props['subject']);
                    $this->em->persist($m);
                    $this->em->flush();
                }
            }
        }catch(\Exception $e){
            echo '<br>';
            echo PHP_EOL;
            echo $e->getMessage();
            echo '<br>';
            echo PHP_EOL;
        }
    }

    public function modelExsists($name){
        $modelRepo = $this->em->getRepository(Mail::class);
        $res = $modelRepo->findOneByName($name);
        return !!$res;
    }


    public function modelList(Request $req){
        $dt = new DatatableRequest($req);
        $modelRepo = $this->em->getRepository(Mail::class);

        $result = $modelRepo->search($dt);

        $mods = array();
        foreach($result as $mod){
            $name = $mod->getName();
            $model = $this->models[$name];
            $m = new MailModelHelper($name);
            $m->setName($model['name']);
            $m->setEvent($model['event']);
            $m->setRecipient($model['recipient']);
            $m->setVariables($model['variables']);
            if(isset($model['from']) && !is_null($model['from'])){
                $m->setFrom($model['from']);
            }
            $m->setText($mod->getText());
            $m->setId($mod->getId());
            $mods[] = $m;
        }
       
        return $mods;
    }

    public function getSingleModel($modelName){
        return $this->models[$modelName];
    }

}