<?php
namespace Mosaika\MailBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\HttpKernel\KernelInterface as BaseKernel;
use Mosaika\MailBundle\Entity\Mail;

class SkinService{
    
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var Kernel
     */
    protected $kernel;

    public function __construct(EntityManagerInterface $em, BaseKernel $kernel)
    {
        $this->em = $em;
        $this->kernel = $kernel;
        $s = Yaml::parse(file_get_contents(__DIR__.'/../Resources/config/skins.yaml'));
        $y = Yaml::parse(file_get_contents($this->kernel->getProjectDir().'/config/packages/mosaika_mail.yaml')) ;
        $this->skins = isset($y['mosaika_mail']['skins']) ? array_merge($s['skins'],$y['mosaika_mail']['skins']) : $s['skins'];
    }

    public function getSkinList(){
        return $this->skins;
    }

    public static function getDefaultSkins(){
        $s = Yaml::parse(file_get_contents(__DIR__.'/../Resources/config/skins.yaml'));
        $skins = $s['skins'];
        return $skins;
    }

    public function getModelSkin($name){
        $repo = $this->em->getRepository(Mail::class);
        $model = $repo->findOneByName($name);
        $skin =  !is_null($model->getSkin()) ? $model->getSkin() : 'default';
        return $this->skins[$skin];
    }



}