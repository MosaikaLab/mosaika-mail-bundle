<?php

namespace App\Utils;

use Symfony\Component\HttpFoundation\Request;

class DatatableRequest {
	protected $start;
	
	protected $limit;
	
	protected $search;
	
	protected $filters;

	protected $orderBy;

	protected $orderDir;

	/**
	 * @param Request $request
	 */
	public function __construct(Request $request, $cols = array()){
		$this->filters = new KBag(json_decode($request->get("filters")));
		$this->start = $request->get("iDisplayStart");
		$this->limit = $request->get("iDisplayLength");
		$this->search = $this->filters->get("s");
		if(!empty($cols)){
			if(strlen($request->get("iSortCol_0")) > 0 ){
				$this->orderBy = $cols[$request->get("iSortCol_0")];
				$this->orderDir = $request->get("sSortDir_0");
			}
		}
	}	
	
	/**
	 * @return mixed
	 */
	public function getStart() {
		return $this->start;
	}

	/**
	 * @param mixed $start
	 * @return DatatableRequest
	 */
	public function setStart($start) {
		$this->start = $start;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getLimit() {
		return $this->limit;
	}

	/**
	 * @param mixed $limit
	 * @return DatatableRequest
	 */
	public function setLimit($limit) {
		$this->limit = $limit;
		return $this;
	}

	/**
	 * @param boolean $likeMode if true, append % to value
	 * @return mixed
	 */
	public function getSearch() {
		return $this->search;
	}

	/**
	 * @param mixed $search
	 * @return DatatableRequest
	 */
	public function setSearch($search) {
		$this->search = $search;
		return $this;
	}
	
	/**
	 * @return \App\Utils\KBag
	 */
	public function getFilters() {
		return $this->filters;
	}
	
	
	/**
	 * @param string $key
	 * @param mixed $defaultValue
	 * @return mixed
	 */
	public function getFilter($k, $defaultValue=null) {
		return $this->filters->get($k, $defaultValue);
	}
	
	/**
	 * @param \App\Utils\KBag $filters
	 * @return DatatableRequest
	 */
	public function setFilters($filters) {
		$this->filters = $filters;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getOrderBy(){
		return $this->orderBy;
	}
	
	/**
	 * @return string
	 */
	public function getOrderDir(){
		return $this->orderDir;
	}
	
	
}

