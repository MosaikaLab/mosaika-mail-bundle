<?php
namespace Mosaika\MailBundle\Model;

abstract class MailModel
{
    /**
	 * @Doctrine\ORM\Mapping\Column(name="id",type="integer")
	 * @Doctrine\ORM\Mapping\Id
	 * @Doctrine\ORM\Mapping\GeneratedValue(strategy="AUTO")
	 * @var integer
	 */
	protected $id;

	/**
	 * @Doctrine\ORM\Mapping\Column(name="`name`",type="string",nullable=true,unique=true,length=127)
	 * @var string
	 */
	protected $name;

	/**
	 * @Doctrine\ORM\Mapping\Column(name="`sender_name`",type="string",nullable=true)
	 * @var string
	 */
	protected $senderName;

	/**
	 * @Doctrine\ORM\Mapping\Column(name="`subject`",type="string",nullable=true)
	 * @var string
	 */
	protected $subject;


	/**
	 * @Doctrine\ORM\Mapping\Column(name="`text`",type="text",nullable=true)
	 * @var string
	 */
	protected $text;

	/**
	 * @Doctrine\ORM\Mapping\Column(name="`skin`",type="string",nullable=true)
	 * @var string
	 */
	protected $skin;


	public function __construct()
	{
	}


	/**
	 * @return self
	 */
	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}


	/**
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}


	/**
	 * @return self
	 */
	public function setName($name)
	{
		$this->name = $name;
		return $this;
	}


	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}


	/**
	 * @return self
	 */
	public function setText($text)
	{
		$this->text = $text;
		return $this;
	}


	/**
	 * @return string
	 */
	public function getText()
	{
		return $this->text;
	}

	/**
	 * Get the value of skin
	 *
	 * @return  string
	 */ 
	public function getSkin()
	{
		return $this->skin;
	}

	/**
	 * Set the value of skin
	 *
	 * @param  string  $skin
	 *
	 * @return  self
	 */ 
	public function setSkin(string $skin)
	{
		$this->skin = $skin;

		return $this;
	}


	public function getSenderName(){
		return $this->senderName;
	}

	public function setSenderName(string $senderName){
		$this->senderName = $senderName;

		return $this;
	}


	public function getSubject(){
		return $this->subject;
	}

	public function setSubject(string $subject){
		$this->subject = $subject;
		return $this;

	}

}