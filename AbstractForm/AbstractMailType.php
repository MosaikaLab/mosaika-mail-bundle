<?php
namespace Mosaika\MailBundle\AbstractForm;

use Symfony\Component\Form\AbstractType;
use Mosaika\MailBundle\Service\SkinService;
use Symfony\Component\Form\FormBuilderInterface;
use Mosaika\MailBundle\Service\ManageModelService;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\ChoiceList\Loader\CallbackChoiceLoader;

abstract class AbstractMailType extends AbstractType{
    public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add("text",TextareaType::class,array())
		->add("skin",ChoiceType::class,array(
			'choice_loader'=> new CallbackChoiceLoader(function(){
				$skins = SkinService::getDefaultSkins();
				$vals = array();
				foreach($skins as $k=>$v){
					$vals[$v['name']] = $k;
				}
				return $vals;
			})
		))
		->add('senderName',null,array())
		->add('subject',null,array())
		;
    }
    
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			"data_class" => "\Mosaika\MailBundle\Entity\Mail",
			"csrf_protection" => false
		));
    }
        
}