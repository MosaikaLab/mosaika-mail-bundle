<?php
namespace Mosaika\MailBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Mosaika\MailBundle\Service\ManageModelService;

class InsertModelsCommand extends Command{

    /**
     * @var ManageModelService
     */
    protected $modelService;

    public function __construct(ManageModelService $modelService)
    {
        parent::__construct();
        $this->modelService = $modelService;
    }

    protected function configure(){
        $this->setName('mk:model:insert');
    }

    protected function execute(InputInterface $input, OutputInterface $output){
        
        $this->modelService->modelsToDb();

        echo 'Inserimento andato a buon fine';

    }
}